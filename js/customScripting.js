
(function( $ ){

	$(document).ready(function() {
		$(".custom-select").selectbox({
			effect: "slide"
		});
		$(".dropzone").dropzone({
			url: "/file/post",
			dictDefaultMessage: "Drag and Drop Team Logo Here"
		});

		$(".brokerage-dropzone").dropzone({
			url: "/file/post",
			dictDefaultMessage: "Drag and Drop Team Logo Here"
		});

		tinymce.init({selector:'#tinymce'});

		$(document).on('click', 'a.js-delete-field', function(e) {
			e.preventDefault();
			if($(this).parent().prev().html() == '&nbsp;') {
				$(this).parents('.form-row').remove();
				$(this).remove();
			} else {
				$(this).prev().remove();
				$(this).remove();
			}

			console.log($(this).parent().prev().html())
		});

		$('.js-add-field').on('click', function(e) {
			e.preventDefault();
			var input = '<div class="form-row clearfix"><div class="form-labels">&nbsp;</div><div class="form-data"><input type="text"><a href="#" class="js-delete-field delete-field"></a></div></div>'
			$(this).parents('.form-row').before(input);
		});

		if($('.donut-chart').length) {
			var data = [66, 34];
			new Chartist.Pie('.donut-chart', {
			  series: data,
				labels: ['66%', '34%']
			}, {
			  donut: true,
			  donutWidth: 10,
			  startAngle: 0,
			  total: 100,
			  showLabel: false,
				chartPadding: 0,
	  		labelOffset: -95,
				labelDirection: 'implode'
			});

			var donutLabels = '<div class="donut-labels"><span class="donut-label-1">' + data[0] + '%</span><span class="donut-label-2">' + data[1] + '%</span></div>'
			$('.donut-chart').append(donutLabels);
		}

// Horizontal chart with 2 bars
		if($('.horizontal-chart').length) {

			var horData = [
				[5, 4, 3, 'automated', 5],
				[3, 2, 9, 'automated', 4]
			]

			var labels = ['Event 1', 'Event 2', 'Event 3', 'Event 4', 'Event 5']

			var horizontalChartTemplate = '<ul class="horizontal-chart-list"></ul>';

			var horizontalChartLine = '<li></li>';

			$('.horizontal-chart').append(horizontalChartTemplate);
			for(var i = 0; i < labels.length; i++) {
				$('.horizontal-chart-list').append('<li><span class="horizontal-chart-list-label">' + labels[i] + '</span><span class="horizontal-chart-list-lines"></span></li>');
			}

			for(var j = 0; j < horData.length; j++) {
				for(var k = 0; k < horData[j].length; k++) {
					if(horData[j][k] == 'automated') {
						$('.horizontal-chart-list li:nth-child(' + (k + 1) + ') .horizontal-chart-list-lines').append('<span class="automated">Automated</span>');
						$('.automated').parent().css('background', '#fff');
					} else {
					$('.horizontal-chart-list li:nth-child(' + (k + 1) + ') .horizontal-chart-list-lines').append('<span class="horizontal-chart-list-line-' + (j + 1) + '"></span>');
					$('.horizontal-chart-list li:nth-child(' + (k + 1) + ') .horizontal-chart-list-lines .horizontal-chart-list-line-' + (j + 1)).css('width', horData[j][k] + '0%');
				}
				}
			}

			$('.horizontal-chart-list li').each(function() {
				var labelWidth = $(this).children('.horizontal-chart-list-label').width() + 20;

				var width = $('.horizontal-chart').width();

				var lineWidth = width - labelWidth;

				$('.horizontal-chart-list-lines').css('width', lineWidth);
			})


			// var horData = [
			// 	[5, 4, 3, 7, 5],
			// 	[3, 2, 9, 5, 4]
			// ]
			//
			// new Chartist.Bar('.horizontal-chart', {
			//   labels: ['Event 1', 'Event 2', 'Event 3', 'Event 4', 'Event 5'],
			//   series: horData
			// }, {
			//   seriesBarDistance: 10,
			//   reverseData: true,
			//   horizontalBars: true,
			//   axisY: {
			//     offset: 80
			//   }
			// });


		}

		if($('.horizontal-chart-full').length) {
			var fullLabels = ['Samantha Byrnes', 'Eduardo da Lopez', 'Simon Chang', 'Alexander Hammond', 'Edwin van der Meyde'];

			var fullData = [
				[96, 67, 120, 24, 130],
				[90, 30, 60, 30, 64],
				[120, 40, 46, 48, 62],
				[20, 30, 15, 14, 28]
			];

			var linesText = [' emails', '% complete', ' events', ' minutes'];

			var horizontalChartFullTemplate = '<ul class="horizontal-chart-full-list"></ul>';

			var horizontalChartFullLine = '<li></li>';

      var red = [];
      var redSumm = 0;
      var green = [];
      var greenSumm = 0;
      var orange = [];
      var orangeSumm = 0;

			$('.horizontal-chart-full').append(horizontalChartFullTemplate);
			for(var a = 0; a < fullLabels.length; a++) {
				$('.horizontal-chart-full-list').append('<li><span class="horizontal-chart-full-list-label">' + fullLabels[a] + '</span><span class="horizontal-chart-full-list-lines"></span></li>');
			}

			for(var b = 0; b < fullData.length; b++) {
				for(var c = 0; c < fullData[j].length; c++) {
					if(fullData[b][c] == 'automated') {
						$('.horizontal-chart-full-list li:nth-child(' + (c + 1) + ') .horizontal-chart-full-list-lines').append('<span class="automated">Automated</span>');
						$('.automated').parent().css('background', '#fff');
					} else {
					$('.horizontal-chart-full-list li:nth-child(' + (c + 1) + ') .horizontal-chart-full-list-lines').append('<div class="horizontal-chart-full-list-line-container clearfix"><span class="horizontal-chart-full-list-line-' + (b + 1) + '"></span></div>');

          if (b === 0) {
            red.push(fullData[b][c]);
            redSumm += fullData[b][c];
          } else if (b === 2) {
            green.push(fullData[b][c]);
            greenSumm += fullData[b][c];
          } else if (b === 3) {
            orange.push(fullData[b][c]);
            orangeSumm += fullData[b][c];
          } else {
            $('.horizontal-chart-full-list li:nth-child(' + (c + 1) + ') .horizontal-chart-full-list-lines .horizontal-chart-full-list-line-' + (b + 1)).css('width', fullData[b][c] + '%');
          }


				}
				}
			}

			for(var d = 0; d < fullData.length; d++) {
				for(var e = 0; e < fullData[d].length; e++) {
					$('.horizontal-chart-full-list li:nth-child(' + (e + 1) + ')  .horizontal-chart-full-list-line-' + (d + 1)).parent().append('<span class="line-text">' + fullData[d][e] + linesText[d] + '</span>');

          if (d === 0) {
            $('.horizontal-chart-full-list li:nth-child(' + (e + 1) + ') .horizontal-chart-full-list-lines .horizontal-chart-full-list-line-' + (d + 1)).css('width', (red[e] * 100) / redSumm + '%');
          } else if (d === 2) {
            $('.horizontal-chart-full-list li:nth-child(' + (e + 1) + ') .horizontal-chart-full-list-lines .horizontal-chart-full-list-line-' + (d + 1)).css('width', (green[e] * 100) / greenSumm + '%');
          } else if (d === 3) {
            $('.horizontal-chart-full-list li:nth-child(' + (e + 1) + ') .horizontal-chart-full-list-lines .horizontal-chart-full-list-line-' + (d + 1)).css('width', (orange[e] * 100) / orangeSumm + '%');
          }
				}
			}

			var labelWidth = [];
			var width = $('.horizontal-chart-full').width();
			$('.horizontal-chart-full-list li').each(function() {
				labelWidth .push($(this).children('.horizontal-chart-full-list-label').width());
			})

			var lineWidth = width - Math.max.apply(Math, labelWidth) - (width / 100) * 7;

			for(var b = 0; b < labelWidth.length; b++) {
				$('.horizontal-chart-full-list li:nth-child(' + (b + 1) + ')  .horizontal-chart-full-list-label').css('margin-right', width - lineWidth - labelWidth[b] - 1);
			}

			$('.horizontal-chart-full-list-lines').css('width', lineWidth);

		}

		if($('.slider-pie-chart').length) {
			var sliderData = [31, 25, 12, 32];
			new Chartist.Pie('.slider-pie-chart', {
				series: sliderData,
				labels: ['31%', '25%', '12%', '32%']
			});
		}

    if($('.slider-pie-chart-2').length) {
			var sliderData2 = [31, 25, 12, 32];
			new Chartist.Pie('.slider-pie-chart-2', {
				series: sliderData2,
				labels: ['31%', '25%', '12%', '32%']
			});
		}

    if($('.slider-pie-chart-4').length) {
			var sliderData4 = [31, 25, 12, 32];
			new Chartist.Pie('.slider-pie-chart-4', {
				series: sliderData4,
				labels: ['31%', '25%', '12%', '32%']
			});
		}

    if($('.slider-pie-chart-6').length) {
			var sliderData6 = [31, 25, 12, 32];
			new Chartist.Pie('.slider-pie-chart-6', {
				series: sliderData6,
				labels: ['31%', '25%', '12%', '32%']
			});
		}

    if($('.slider-pie-chart-8').length) {
			var sliderData8 = [31, 25, 12, 32];
			new Chartist.Pie('.slider-pie-chart-8', {
				series: sliderData8,
				labels: ['31%', '25%', '12%', '32%']
			});
		}


		if($('.email-donut').length) {
			var emailData = [77];
			var emailLabels = ['77%'];
			new Chartist.Pie('.email-donut', {
			  series: emailData,
				labels: emailLabels
			}, {
			  donut: true,
			  donutWidth: 30,
			  startAngle: 0,
			  total: 100,
				showLabel: false
			});

			$('.email-donut').append('<span class="email-donut-label">' + emailLabels + '</span>')

			var emailDataSecond = [23];
			var emailLabelsSecond = ['23%'];
			new Chartist.Pie('.email-donut-second', {
			  series: emailDataSecond,
				labels: emailLabelsSecond
			}, {
			  donut: true,
			  donutWidth: 30,
			  startAngle: -83,
			  total: 100,
			  showLabel: false
			});

			$('.email-donut-second').append('<span class="email-donut-second-label">' + emailLabelsSecond + '</span>')
		}

    if($('.email-donut-3').length) {
			var emailData3 = [77];
			var emailLabels3 = ['77%'];
			new Chartist.Pie('.email-donut-3', {
			  series: emailData3,
				labels: emailLabels3
			}, {
			  donut: true,
			  donutWidth: 30,
			  startAngle: 0,
			  total: 100,
				showLabel: false
			});

			$('.email-donut-3').append('<span class="email-donut-label">' + emailLabels + '</span>')

			var emailDataSecond3 = [23];
			var emailLabelsSecond3 = ['23%'];
			new Chartist.Pie('.email-donut-second-3', {
			  series: emailDataSecond3,
				labels: emailLabelsSecond3
			}, {
			  donut: true,
			  donutWidth: 30,
			  startAngle: -83,
			  total: 100,
			  showLabel: false
			});

			$('.email-donut-second-3').append('<span class="email-donut-second-label">' + emailLabelsSecond + '</span>')
		}

    if($('.email-donut-2').length) {
			var emailData2 = [77];
			var emailLabels2 = ['77%'];
			new Chartist.Pie('.email-donut-2', {
			  series: emailData2,
				labels: emailLabels2
			}, {
			  donut: true,
			  donutWidth: 30,
			  startAngle: 0,
			  total: 100,
				showLabel: false
			});

			$('.email-donut-2').append('<span class="email-donut-label">' + emailLabels + '</span>')

			var emailDataSecond2 = [23];
			var emailLabelsSecond2 = ['23%'];
			new Chartist.Pie('.email-donut-second-2', {
			  series: emailDataSecond2,
				labels: emailLabelsSecond2
			}, {
			  donut: true,
			  donutWidth: 30,
			  startAngle: -83,
			  total: 100,
			  showLabel: false
			});

			$('.email-donut-second-2').append('<span class="email-donut-second-label">' + emailLabelsSecond + '</span>')
		}

    if($('.email-donut-4').length) {
			var emailData4 = [77];
			var emailLabels4 = ['77%'];
			new Chartist.Pie('.email-donut-4', {
			  series: emailData4,
				labels: emailLabels4
			}, {
			  donut: true,
			  donutWidth: 30,
			  startAngle: 0,
			  total: 100,
				showLabel: false
			});

			$('.email-donut-4').append('<span class="email-donut-label">' + emailLabels + '</span>')

			var emailDataSecond4 = [23];
			var emailLabelsSecond4 = ['23%'];
			new Chartist.Pie('.email-donut-second-4', {
			  series: emailDataSecond4,
				labels: emailLabelsSecond4
			}, {
			  donut: true,
			  donutWidth: 30,
			  startAngle: -83,
			  total: 100,
			  showLabel: false
			});

			$('.email-donut-second-4').append('<span class="email-donut-second-label">' + emailLabelsSecond + '</span>')
		}

    if($('.email-donut-5').length) {
			var emailData5 = [77];
			var emailLabels5 = ['77%'];
			new Chartist.Pie('.email-donut-5', {
			  series: emailData5,
				labels: emailLabels5
			}, {
			  donut: true,
			  donutWidth: 30,
			  startAngle: 0,
			  total: 100,
				showLabel: false
			});

			$('.email-donut-5').append('<span class="email-donut-label">' + emailLabels + '</span>')

			var emailDataSecond5 = [23];
			var emailLabelsSecond5 = ['23%'];
			new Chartist.Pie('.email-donut-second-5', {
			  series: emailDataSecond5,
				labels: emailLabelsSecond5
			}, {
			  donut: true,
			  donutWidth: 30,
			  startAngle: -83,
			  total: 100,
			  showLabel: false
			});

			$('.email-donut-second-5').append('<span class="email-donut-second-label">' + emailLabelsSecond + '</span>')
		}

		if($('.slider-pie-chart-1').length) {
			var sliderData1 = [50, 32, 18];
			new Chartist.Pie('.slider-pie-chart-1', {
				series: sliderData1,
				labels: ['50%', '32%', '18%']
			});
		}

    if($('.slider-pie-chart-3').length) {
			var sliderData3 = [50, 32, 18];
			new Chartist.Pie('.slider-pie-chart-3', {
				series: sliderData3,
				labels: ['50%', '32%', '18%']
			});
		}

    if($('.slider-pie-chart-5').length) {
			var sliderData5 = [50, 32, 18];
			new Chartist.Pie('.slider-pie-chart-5', {
				series: sliderData5,
				labels: ['50%', '32%', '18%']
			});
		}

    if($('.slider-pie-chart-7').length) {
			var sliderData7 = [50, 32, 18];
			new Chartist.Pie('.slider-pie-chart-7', {
				series: sliderData7,
				labels: ['50%', '32%', '18%']
			});
		}

    if($('.slider-pie-chart-9').length) {
			var sliderData9 = [50, 32, 18];
			new Chartist.Pie('.slider-pie-chart-9', {
				series: sliderData9,
				labels: ['50%', '32%', '18%']
			});
		}


		if($('.chart-slider').length) {
			$('.bx-slider').bxSlider();
		}

  var sliderHeadersWidth = [];
  $('.bx-slider h3').each (function () {
    var headerWidth = $(this).textWidth();
    sliderHeadersWidth.push(headerWidth);
  });

    var maxHeader = Math.max.apply(Math, sliderHeadersWidth);
    console.log(maxHeader);

    $('.bx-wrapper .bx-prev').css({
      'margin-left': ($('.bx-wrapper').width() - maxHeader)/2 - 82 + 'px'
    });

    $('.bx-wrapper .bx-next').css({
      'margin-right': ($('.bx-wrapper').width() - maxHeader)/2 - 82 + 'px'
    });

	})

  $.fn.textWidth = function(text, font) {
    if (!$.fn.textWidth.fakeEl) $.fn.textWidth.fakeEl = $('<span>').appendTo(document.body);
    var htmlText = text || this.val() || this.text();
    htmlText = $.fn.textWidth.fakeEl.text(htmlText).html(); //encode to Html
    htmlText = htmlText.replace(/\s/g, "&nbsp;"); //replace trailing and leading spaces
    $.fn.textWidth.fakeEl.html(htmlText).css('font', font || this.css('font'));
    return $.fn.textWidth.fakeEl.width();
};

})( jQuery )
